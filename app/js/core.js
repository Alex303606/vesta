$(document).ready(function() {
   	$('#btn-catalog').on('click',function(){
   		$('.dropdown-catalog').slideToggle();
   	});

   	$('.main-slider').slick({
   		arrows: false,
   		dots: true
   	}); 

   	$('a.order-call').fancybox();

   	//Счетчик
    var btnIncrease = $('a.minus'),
		btnDecrease = $('a.plus'),
		counter = $('.count');

	
	btnIncrease.click(function(e){
		// console.log('-');
		e.preventDefault();
		var input = $(this).next('.count');
		decrementCounter(input);
	});

	btnDecrease.click(function(e){
		// console.log('+');
		e.preventDefault();
		var input = $(this).prev('.count');
		incrementCounter(input);
	});

	// Не позволять вводить символы кроме цифр
	counter.keyup(function(e)
	{
		if (/\D/g.test(this.value))
		{
		    this.value = this.value.replace(/\D/g, '');
		}
	});

	// Увеличиваем счетчик
	function incrementCounter(input){
		var prevVal = parseInt(input.val());
		$(input).val(prevVal+1);
	}

	// Уменьшаем счетчик
	function decrementCounter(input){
		var prevVal = parseInt(input.val());
		if(prevVal>0){
			$(input).val(prevVal-1);
		}
	}	

	$('.open-subsection span.title').on('click',function(){
		$('.open-subsection span.title').not(this).removeClass('active');
		$(this).toggleClass('active');
		if($(this).hasClass('active')) {
			$('.open-subsection').find('.scroll-wrapper').hide();
			$(this).closest('.open-subsection').find('.scroll-wrapper').slideDown();
		} else {
			$(this).closest('.open-subsection').find('.scroll-wrapper').slideUp();
		}
	});
	
	$('.scrollbar-inner').scrollbar();

	$('button.open-dropdown').on('click',function(){
		$(this).closest('.order-wrap').find('.dropdown-order').slideDown();				
	});

	$('button.close-dropdown').on('click',function(){
		$(this).closest('.order-wrap').find('.dropdown-order').slideUp();
	});

	$('.spec-slider').slick({
		dots: false,
		arrows: true,
		slidesToScroll: 3,
		slidesToShow: 6,
		responsive: [
	    {
	      breakpoint: 1280,
	      settings: {
	        slidesToShow: 5,
	        slidesToScroll: 2	        
	      }
	    },
	    {
	      breakpoint: 1080,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 900,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1
	      }
	    },	    
	    {
	      breakpoint: 650,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});

	$('.show-sidebar').on('click',function(){
		$('.hide-mobile').slideToggle();
		$(this).toggleClass('active');
	});

	$('.title-open-dropdown').on('click',function(){
		$(this).toggleClass('active').next('.scroll-wrapper.dropdown.scrollbar-inner').slideToggle();
	});

	$('.price .title-open-dropdown').on('click',function(){
		$(this).toggleClass('active').next('.dropdown').slideToggle();
	});

	$(document).on('click','button.remove',function(){
		if($(this).closest('ul').find('li').length <= 8) {
			var height = $(this).closest('.scroll-wrapper').height();
			height = height - 32;
			$(this).closest('.scroll-wrapper').css('height',height);			
		}
		$(this).closest('li').remove();		
	});	

	$('#btn-down_overflow').on('click',function(){
		$('.scroll-wrapper.scrollbar-inner.wrap').slideToggle();
	});
	
	if($(window).width() > 1024) {
		$('a.more').hover(mouseenter,mouseover);

		function mouseenter(){
			$(this).closest('.item').find('.dropdown-item-block').show();
		}

		function mouseover(){
			$(this).closest('.item').find('.dropdown-item-block').hide();
		}		
	}

});


